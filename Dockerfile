FROM ubuntu:20.04

ENV DEBIAN_FRONTEND noninteractive

# Install the basics
RUN apt-get clean && apt-get update \
    && apt-get install -y python3-pip gdal-bin python3-pyproj \
    build-essential wget ca-certificates postgresql postgresql-contrib \
    postgis

COPY requirements.txt /tmp/
COPY entrypoint.sh /usr/local/bin/

# Install PIP requirements
RUN pip3 install --upgrade -r /tmp/requirements.txt

# Set up our run environment
RUN mkdir -p /opt/reporter
COPY python /opt/reporter/
COPY sql /opt/reporter/
COPY data /opt/reporter/

# This will get overwritten in the Docker Compose
# COPY dbconfig_template.py /opt/reverberator/dbconfig.py
WORKDIR /opt/reporter

ENTRYPOINT entrypoint.sh
CMD python3 --version
