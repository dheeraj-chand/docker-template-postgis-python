up:
	docker-compose up -d

build:
	docker-compose stop
	docker-compose build
	
# Shouldn't be required
make-folders:
	mkdir -p data/census
	mkdir -p data/output

clean:
	docker-compose down

shell:
	docker-compose exec python bash
